import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { mount } from 'enzyme';

import App from './components/App';
import ticketFixtures from './reducers/fixtures/ticketFixtures';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ tickets: ticketFixtures });

describe('App Container should render with ticket-issues data', () => {
  it('should pass', () => {
    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(wrapper).toBeDefined();
    expect(wrapper.find('li[data-status="To Do"]')).toHaveLength(1);
    expect(wrapper.find('li[data-status="In Progress"]')).toHaveLength(1);
    expect(wrapper.find('li[data-status="Done"]')).toHaveLength(1);
  });
});
