import React from 'react';
import PropTypes from 'prop-types';

class ModalPrompt extends React.Component {
  constructor(props) {
    super(props);

    this.onCreateIssue = this.onCreateIssue.bind(this);
  }

  onCreateIssue(e) {
    const { onCreateIssue } = this.props;
    onCreateIssue({ e, summary: this.refs.summary.value, description: this.refs.description.value });
  }

  render() {
    return (
      <div id="bs-modal" className="bs-modal modal modal-center fade" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="bs-close close" data-dismiss="modal">
                <span className="glyphicon glyphicon-remove-circle" />
              </button>
              <h4 className="modal-title">Create Issue</h4>
            </div>
            <div className="modal-body">
              <form className="form-horizontal">
                <div className="form-group">
                  <label className="control-label col-sm-2" htmlFor="summary">
                    summary:
                  </label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control" id="summary" placeholder="Enter summary" name="summary" ref="summary" />
                  </div>
                </div>
                <div className="form-group">
                  <label className="control-label col-sm-2" htmlFor="description">
                    description:
                  </label>
                  <div className="col-sm-10">
                    <textarea
                      rows="5"
                      type="text"
                      className="form-control"
                      id="description"
                      placeholder="Enter description"
                      name="description"
                      ref="description"
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button onClick={this.onCreateIssue} type="button" className="bs-yes btn btn-primary" data-dismiss="modal">
                Create
              </button>
              <button type="button" className="bs-no btn btn-default" data-dismiss="modal">
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ModalPrompt.propTypes = {
  dispatch: PropTypes.func,
  onCreateIssue: PropTypes.func
};

export default ModalPrompt;
