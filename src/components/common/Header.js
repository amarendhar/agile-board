import React from 'react';

const Header = props => {
  return (
    <nav className="navbar navbar-default">
      <div className="container-fluid">
        <div className="navbar-header">
          <a className="navbar-brand" href="#">
            Agile Board
          </a>
        </div>
        <button className="btn btn-danger navbar-btn" data-toggle="modal" data-target="#bs-modal">
          Create
        </button>
      </div>
    </nav>
  );
};

export default Header;
