import React from 'react';
import styled from 'styled-components';
import { findDOMNode, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import dragula from 'react-dragula';

import Header from './common/Header';
import ItemContainer from './ItemContainer';
import ModalPrompt from './common/ModalPrompt';
import { getTickets } from '../actions';
import GlobalStyles from '../GlobalStyles';

import storage from '../helpers/StorageHelper';

const Container = styled.div`
  position: relative;
`;

const Columns = styled.ul`
  box-sizing: border-box;
  border-spacing: 10px 0;
  display: table;
  list-style: none;
  margin: 0;
  table-layout: fixed;
  width: 100%;
  padding: 0;
`;

const Column = styled.li`
  background: #f5f5f5;
  box-sizing: border-box;
  display: table-cell;
  list-style: none;
  margin: 0;
  padding: 1px 0 0 !important;
  position: relative;
  vertical-align: top;
  // overriding bootstrap css
  float: none !important;
`;

class App extends React.Component {
  constructor(props) {
    super(props);

    const { columns = [], issues = [] } = props;

    this.state = {
      columns,
      issues
    };

    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.onDrag = this.onDrag.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.onDragend = this.onDragend.bind(this);
    this.onCreateIssue = this.onCreateIssue.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    // Here getTickets() function can be replaced with API call.
    dispatch(getTickets());
  }

  componentWillReceiveProps(nextProps) {
    const { columns, issues } = nextProps;
    if (this.props.columns !== columns) {
      storage.set('tickets', { columns, issues });
      this.setState({
        columns,
        issues
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.columns !== this.props.columns && this.ToDo) {
      dragula([this.ToDo, this.InProgress, this.Done], {
        removeOnSpill: true
      })
        .on('drop', this.onDrop)
        .on('drag', this.onDrag)
        .on('dragend', this.onDragend);
    }
  }

  handleRemoveItem({ column, index, title, summary }) {
    const { issues = [], columns = [] } = this.state;
    const issue = issues[index];
    if (issue && issue.title === title) {
      issues.splice(index, 1);
      // Can do API call to remove the issue from the Issues List.
      storage.set('tickets', { columns, issues });
      this.setState({
        issues
      });
    }
  }

  onDrag(el, source) {
    const DOMissues = document.querySelectorAll('.issue');
    this.fromIndex = [].indexOf.call(DOMissues, el);
  }

  onDrop(el, target, source, sibling) {
    if (el && target) {
      const DOMissues = document.querySelectorAll('.issue:not(.gu-mirror)');
      const toIndex = [].indexOf.call(DOMissues, el);
      const { title, status } = el.dataset;
      const { status: targetStatus } = target.dataset;
      this.issues = this.issues || this.state.issues;
      const issue = this.issues[this.fromIndex];

      if (issue && issue.title === title) {
        issue.status = targetStatus;
        this.issues.splice(this.fromIndex, 1);
        this.issues.splice(toIndex, 0, issue);
        // Can do API call to update an issue in Issues List.
        storage.set('tickets', { columns: this.state.columns, issues: this.issues });
      }
    }
  }

  onDragend(el) {
    // console.log('el -> ', el);
  }

  onCreateIssue({ summary, description }) {
    const { issues, columns } = this.state;
    const toIndex = document.querySelectorAll('.issue[data-status="To Do"]');
    
    issues.splice(toIndex, 0, {
      title: `ADAPT-${Math.floor(Math.random() * Math.floor(100))}`,
      status: 'To Do',
      summary
    });

    // Can do API call to add an issue into Issues List.
    storage.set('tickets', { columns, issues });
    this.setState({ issues });
  }

  render() {
    const { columns = [], issues = [] } = this.state;

    return (
      <GlobalStyles>
        <Header />
        <Container>
          <Columns>
            {columns.map(columnName => (
              <Column data-status={columnName} key={columnName} className="col-xs-4" innerRef={el => (this[columnName.replace(' ', '')] = el)}>
                <div>{columnName}</div>
                {this.state.issues.map(
                  (item, key) =>
                    item.status === columnName && (
                      <ItemContainer
                        className="issue"
                        data-index={key}
                        data-title={item.title}
                        data-status={item.status}
                        index={key}
                        key={`${item.title}-${key}`}
                        role="listitem"
                        tabIndex="0"
                        column={columnName}
                        handleRemoveItem={this.handleRemoveItem}
                        {...item}
                      />
                    )
                )}
              </Column>
            ))}
          </Columns>
        </Container>
        <ModalPrompt onCreateIssue={this.onCreateIssue} />
      </GlobalStyles>
    );
  }
}

App.propTypes = {
  dispatch: PropTypes.func,
  columns: PropTypes.array,
  issues: PropTypes.array
};

function mapStateToProps(state, ownProps) {
  const { tickets = {} } = state;
  const { columns = [], issues = [] } = tickets;
  return {
    columns,
    issues
  };
}

export default connect(mapStateToProps)(App);
