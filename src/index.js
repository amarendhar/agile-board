import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { BrowserRouter as Router, StaticRouter, Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import routes from './routes/routes';
import configureStore from './store/configureStore';

import storage from './helpers/StorageHelper';

/**
 * Order of importing these css files are also very important
 * here global.scss styles will be overriden on top of bootstrap.min.css
 * here helper.scss styles will be overriden on top of global.scss & bootstrap.min.css
 */
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

import '../node_modules/jquery/dist/jquery.min.js';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';

const history = createBrowserHistory();
const store = configureStore(history);
console.log('storage.get(tickets)', storage.get('tickets'));
console.log('src/index.js : store.getState() -> ', store.getState());

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>{routes}</ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
