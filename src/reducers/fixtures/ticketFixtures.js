export default {
  columns: ['To Do', 'In Progress', 'Done'],
  issues: [
    {
      title: 'ADAPT-1234',
      status: 'To Do',
      summary: 'ratings & reviews on PDP-page'
    },
    {
      title: 'ADAPT-2234',
      status: 'To Do',
      summary: 'inline recs'
    },
    {
      title: 'ADAPT-3234',
      status: 'To Do',
      summary: 'crush functionality'
    },
    {
      title: 'ADAPT-4234',
      status: 'In Progress',
      summary: 'ratings & reviews on All-Reviews-page'
    },
    {
      title: 'ADAPT-5234',
      status: 'In Progress',
      summary: 'mini cart menu'
    },
    {
      title: 'ADAPT-6234',
      status: 'Done',
      summary: 'your recs page'
    }
  ]
};
