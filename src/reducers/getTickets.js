import storage from '../helpers/StorageHelper';
import ticketFixtures from './fixtures/ticketFixtures';

const initialState = {};

export default function getTickets(state = initialState, action) {
  switch (action.type) {
    case 'GET_TICKETS':
      return Object.assign({}, state, storage.get('tickets') || ticketFixtures);
    default:
      return state;
  }
}
