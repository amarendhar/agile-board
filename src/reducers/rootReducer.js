import { combineReducers } from 'redux';
import { combineForms, createForms } from 'react-redux-form';
// import { formReducer } from 'react-redux-form';

import getTickets from './getTickets';

const rootReducer = combineReducers({
  tickets: getTickets
});

export default rootReducer;
