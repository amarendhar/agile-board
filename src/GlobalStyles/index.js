import React from 'react';
import PropTypes from 'prop-types';
import { injectGlobal } from 'styled-components';
import dragDropCSS from './dragDropCSS';

const GlobalStyles = ({ children, ...restProps }) => {
  injectGlobal`
    ${dragDropCSS}
  `;

  return <div {...restProps}>{children}</div>;
};

GlobalStyles.propTypes = {
  children: PropTypes.node
};

export default GlobalStyles;
